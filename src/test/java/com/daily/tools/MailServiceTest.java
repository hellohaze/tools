package com.daily.tools;

import com.daily.tools.service.MailService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MailServiceTest {

    private String to = "nagepoptang@163.com";

    @Autowired
    private MailService mailService;

    @Autowired
    private TemplateEngine templateEngine;

    @Test
    public void testSimpleMail() {
        String subject = "test simple mail";
        String content = "hello this is simple mail";
        mailService.sendSimpleMail(to, subject, content);
    }

    @Test
    public void testHtmlMail() {
        String subject = "test html mail";
        String content = "<html>\n" +
                "<body>\n" +
                "    <h3>hello world! 这是一封Html邮件!</h3>\n" +
                "</body>\n" +
                "</html>";
        mailService.sendHtmlMail(to, subject, content);
    }

    @Test
    public void sendTemplateMail() {
        //创建邮件正文
        Context context = new Context();
        context.setVariable("id", "006");
        String emailContent = templateEngine.process("emailTemplate", context);

        String subject = "主题：这是模板邮件";

        mailService.sendHtmlMail(to, subject, emailContent);
    }

    @Test
    public void sendAttachmentsMail() {
        String filePath = "E:\\ichujian\\static\\hxpc\\contract\\2018\\11\\09\\D181120158455019.pdf";
        String subject = "主题：带附件的邮件";
        mailService.sendAttachmentsMail(to, subject, "有附件，请查收！", filePath);
    }

    @Test
    public void sendInlineResourceMail() {
        String rscId = "neo006";
        String content = "<html>\n" +
                "<body>\n" +
                "这是有图片的邮件：<img src=\\'cid:\" + rscId + \"\\' >\\n" +
                "</body>\n" +
                "</html>";
        String subject = "主题：这是有图片的邮件";
        String rscPath = "E:\\ichujian\\static\\hxpc\\img\\hoocial.png";
        mailService.sendInlineResourceMail(to, subject, content, rscPath, rscId);
    }
}