package com.daily.tools;

import com.alibaba.excel.EasyExcelFactory;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.metadata.*;
import com.daily.tools.bean.WriteModel;
import com.daily.tools.bean.WriteModel2;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EasyExcelTest {

    @Test
    public void writeExcel() throws IOException {
        FileOutputStream out = new FileOutputStream("C:\\Users\\Administrator\\Desktop\\easyexcel.xlsx");
        ExcelWriter writer = EasyExcelFactory.getWriter(out);

        Sheet sheet = new Sheet(1, 0, WriteModel.class);
        sheet.setSheetName("第一个sheet");

        /**
         * 参数1：创建要写入的数据模型
         * 参数2：要写入的目标sheet
         */
        writer.write(createModelList(), sheet);

        writer.finish();
        out.close();
    }

    // 数据
    private List<? extends BaseRowModel> createModelList() {
        List<WriteModel> rows = Lists.newArrayList();

        for (int i = 0; i < 50; i++) {
            WriteModel writeModel = WriteModel.builder()
                    .name("张三_" + i)
                    .password("password_" + i)
                    .age(10 + i)
                    .build();

            rows.add(writeModel);
        }

        return rows;
    }

    @Test
    public void writeExcel2() throws IOException {
        FileOutputStream out = new FileOutputStream("C:\\Users\\Administrator\\Desktop\\easyexcel2.xlsx");
        ExcelWriter writer = EasyExcelFactory.getWriter(out);

        // 动态添加表头
        Sheet sheet = new Sheet(1, 0);

        sheet.setSheetName("第一个sheet");

        // 创建一个表格，用于sheet
        Table table = new Table(1);

        table.setTableStyle(createTableStyle());// 样式
        table.setHead(createTestListStringHead());// 表头

        writer.write1(createDynamicModelList(), sheet, table);

        // 合并单元格
//        writer.merge(5, 6, 0, 4);// 合并第6行-第7行，第1列-第5列

        writer.finish();
        out.close();

    }

    /**
     * 设置表格样式
     *
     * @return
     */
    private TableStyle createTableStyle() {
        TableStyle tableStyle = new TableStyle();

        // 表头样式
        Font headFont = new Font();
        headFont.setBold(true);
        headFont.setFontHeightInPoints((short) 12);
        headFont.setFontName("黑体");

        tableStyle.setTableHeadFont(headFont);
        tableStyle.setTableHeadBackGroundColor(IndexedColors.BLUE);

        // 正文样式
        Font contentFont = new Font();
        contentFont.setFontHeightInPoints((short) 10);
        contentFont.setFontName("楷体");
        tableStyle.setTableContentFont(contentFont);
        tableStyle.setTableContentBackGroundColor(IndexedColors.GREEN);

        return tableStyle;

    }

    /**
     * 设置表头
     *
     * @return
     */
    private List<List<String>> createTestListStringHead() {
        List<List<String>> head = Lists.newArrayList();
        List<String> headColumn1 = Lists.newArrayList();
        List<String> headColumn2 = Lists.newArrayList();
        List<String> headColumn3 = Lists.newArrayList();
        List<String> headColumn4 = Lists.newArrayList();
        List<String> headColumn5 = Lists.newArrayList();

        headColumn1.add("第一列");
        headColumn1.add("第一列");
        headColumn1.add("第一列");

        headColumn2.add("第一列");
        headColumn2.add("第一列");
        headColumn2.add("第一列");

        headColumn3.add("第二列");
        headColumn3.add("第二列");
        headColumn3.add("第二列");

        headColumn4.add("第三列");
        headColumn4.add("第三列2");
        headColumn4.add("第三列2");

        headColumn5.add("第一列");
        headColumn5.add("第三列");
        headColumn5.add("第四列");

        head.add(headColumn1);
        head.add(headColumn2);
        head.add(headColumn3);
        head.add(headColumn4);
        head.add(headColumn5);
        return head;
    }

    /**
     * 生成动态数据
     *
     * @return
     */
    private List<List<Object>> createDynamicModelList() {
        List<List<Object>> rows = Lists.newArrayList();

        for (int i = 0; i < 50; i++) {
            List<Object> row = Lists.newArrayList();
            row.add("字符串" + i);
            row.add(123089L + i);
            row.add(123123 + i);
            row.add("合并单元格1");
            row.add("合并单元格2");

            rows.add(row);
        }

        return rows;
    }

    /**
     * Invalid row number (1048576) outside allowable range (0..1048575)
     * 07 版 excel 单个 sheet 允许的最大行数是 1048576
     *
     * @throws Exception
     */
    @Test
    public void writeChongqingExcel() throws Exception {
        OutputStream out = new FileOutputStream("C:\\Users\\Administrator\\Desktop\\chongqing.xlsx");
        ExcelWriter writer = EasyExcelFactory.getWriter(out);

        Sheet sheet1 = new Sheet(1, 0);
        sheet1.setSheetName("电子超市标准版商品导出");
        // 设置列的宽度
//        Map columnWidth = new HashMap();
//        columnWidth.put(0, 10);
//        sheet1.setColumnWidthMap(columnWidth);
//        sheet1.setAutoWidth(true);
        Table table1 = new Table(1);

        //
        // 自定义表格样式
        table1.setTableStyle(createChongqingTableStyle());
//        // 无注解的模式，动态添加表头
        table1.setHead(createTestListStringHead());

        // 内容
        for (int i = 0; i < 2; i++) {
            writer.write1(createChongqingDataList(), sheet1, table1);
        }

        // 最后一行，总价合计
        List<List<Object>> lastLine = new ArrayList<>();
        List<Object> objects = new ArrayList<Object>();
        objects.add("总价合计：21");
        lastLine.add(objects);
        writer.write1(lastLine, sheet1, table1);

        // 合并单元格 todo
        writer.merge(12, 12, 0, 11);


        writer.finish();
        out.close();
    }

    public static TableStyle createChongqingTableStyle() {
        // 表头样式
        TableStyle tableStyle = new TableStyle();
        Font headFont = new Font();
        headFont.setBold(true);
        headFont.setFontHeightInPoints((short) 12);
        headFont.setFontName("微软雅黑");
        tableStyle.setTableHeadFont(headFont);
        // 内容背景色
        tableStyle.setTableHeadBackGroundColor(IndexedColors.BLUE1);

        // 内容样式
        Font contentFont = new Font();
        contentFont.setFontHeightInPoints((short) 12);
        contentFont.setFontName("微软雅黑");
        tableStyle.setTableContentFont(contentFont);
        // 内容背景色
        tableStyle.setTableContentBackGroundColor(IndexedColors.WHITE);

        return tableStyle;
    }

    /**
     * 无注解的实体类
     *
     * @return
     */
    private List<List<Object>> createChongqingDataList() {
        List<List<Object>> datas = Lists.newArrayList();

        for (int i = 0; i < 90; i++) {
            List<Object> objects = new ArrayList<>();
            objects.add(i + 1);
            objects.add("3P5072 全日安 白色涤纶防静电手套 8\" A03C/AS 8\" 包装内件数：12副/打 销售单位：打『固安捷』");
            objects.add("79446");
            objects.add("3P5072" + i);
            objects.add("");
            objects.add("");
            objects.add("");
            objects.add("100050");
            objects.add(-1);
            objects.add(21);
            objects.add(i);
            objects.add(21);
            datas.add(objects);
        }

        return datas;
    }

    @Test
    public void writeExcelSimple() throws Exception {
        OutputStream out = new FileOutputStream("C:\\Users\\Administrator\\Desktop\\test.xlsx");
        ExcelWriter writer = EasyExcelFactory.getWriter(out);
        // ==================================== Start ====================================
        // 写仅有一个 Sheet 的 Excel, 此场景较为通用
        Sheet sheet1 = new Sheet(1, 0, WriteModel2.class);
        sheet1.setSheetName("第一个sheet");
        writer.write(createModel2List(), sheet1);
        // ===================================== End =====================================





        // ==================================== Start ====================================
        // 合并单元格
        Sheet sheet3 = new Sheet(3, 0, WriteModel.class, "第三个sheet", null);
        //writer.write1(null, sheet2);
        writer.write(createModelList(), sheet3);
        // 需要合并单元格
        writer.merge(5, 6, 1, 5);
        // ===================================== End =====================================


        // ==================================== Start ====================================
        // 单个 Sheet 中包含多个 Table
        Sheet sheet4 = new Sheet(4, 0);
        sheet4.setSheetName("第四个sheet");

        Table sheet4table1 = new Table(1);
        sheet4table1.setClazz(WriteModel.class);
        writer.write(createModelList(), sheet4, sheet4table1);

        Table sheet4table2 = new Table(2);
        sheet4table2.setClazz(WriteModel2.class);
        writer.write(createModel2List(), sheet4, sheet4table2);
        // ===================================== End =====================================

        writer.finish();
        out.close();
    }

    private List<WriteModel2> createModel2List() {
        List<WriteModel2> writeModels2 = Lists.newArrayList();

        for (int i = 0; i < 100; i++) {
            WriteModel2 writeModel2 = WriteModel2.builder().orderNo(String.valueOf(i)).name("犬小哈").createTime(LocalDateTime.now()).build();
            writeModels2.add(writeModel2);
        }

        return writeModels2;
    }

}
