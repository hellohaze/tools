package com.daily.tools.service.impl;

import com.daily.tools.bean.OrderDto;
import com.daily.tools.handler.AbstractHandler;
import com.daily.tools.handler.HandlerContext;
import com.daily.tools.handler.massage.CancelOrderSender;
import com.daily.tools.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 策略模式：test service
 *
 * @Author: Mali
 * @Date: 2019/9/19 14:58
 * @Version 1.0
 */
@Service
@Slf4j
public class OrderServiceImpl implements OrderService {

    @Autowired
    private HandlerContext handlerContext;

    @Autowired
    private CancelOrderSender cancelOrderSender;

    @Override
    public String handleOrder(OrderDto orderDto) {
        AbstractHandler handler = handlerContext.getInstance(orderDto.getType());
        return handler.handle(orderDto);
    }

    @Override
    public void createOrder(OrderDto orderDto) {
        log.debug("创建订单，订单编号：" + orderDto.getCode());

        // 延迟时间（一般为创建订单后1个小时未支付就取消，这里为了方便测试设置为20秒）
        long delayTime = 20 * 1000;
        // 发送定时取消订单的消息到消息队列
        cancelOrderSender.sendMessage(orderDto.getCode(), delayTime);
    }

    @Override
    public void cancelOrder(String code) {
        log.debug("订单过期未支付，删除订单。订单编号：" + code);
    }

}
