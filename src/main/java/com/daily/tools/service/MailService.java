package com.daily.tools.service;

import java.io.File;

/**
 * Description:
 * Author: Mali
 * Date: 2018/11/22 14:46
 * Version:1.0
 **/
public interface MailService {

    /**
     * 简单邮件
     *
     * @param to      收件人邮箱
     * @param subject 邮件标题
     * @param content 邮件内容
     */
    void sendSimpleMail(String to, String subject, String content);

    /**
     * HTML邮件
     *
     * @param to      收件人邮箱
     * @param subject 邮件标题
     * @param content 邮件内容
     */
    void sendHtmlMail(String to, String subject, String content);

    /**
     * 带附件的邮件
     *
     * @param to       收件人邮箱
     * @param subject  邮件标题
     * @param content  邮件内容
     * @param filePath 文件路径
     */
    void sendAttachmentsMail(String to, String subject, String content, String filePath);

    /**
     * 带附件的邮件
     *
     * @param to       收件人邮箱
     * @param subject  邮件标题
     * @param content  邮件内容
     * @param fileName 文件名
     * @param file     文件
     */
    void sendAttachmentsMail(String to, String subject, String content, String fileName, File file);

    /**
     * 嵌入静态资源的邮件
     *
     * @param to      收件人邮箱
     * @param subject 邮件标题
     * @param content 邮件内容
     * @param rscPath 静态资源地址
     * @param rscId   静态资源在邮件内容中的标签上的id属性（例如：邮件内容中有一张图片，则img标签的id属性即该参数）
     */
    void sendInlineResourceMail(String to, String subject, String content, String rscPath, String rscId);
}
