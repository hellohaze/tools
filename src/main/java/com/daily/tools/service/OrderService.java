package com.daily.tools.service;

import com.daily.tools.bean.OrderDto;

/**
 * 策略模式test service
 *
 * @Author: Mali
 * @Date: 2019/9/19 14:58
 * @Version 1.0
 */
public interface OrderService {

    String handleOrder(OrderDto orderDto);

    void createOrder(OrderDto orderDto);

    void cancelOrder(String code);
}
