package com.daily.tools.handler;

import com.daily.tools.bean.OrderDto;

/**
 * 策略模式：处理业务逻辑抽象类，所有具体的操作类都继承此类
 * @Author: Mali
 * @Date: 2019/9/19 15:03
 * @Version 1.0
 */
public abstract class AbstractHandler {

    abstract public String handle(OrderDto orderDto);
}
