package com.daily.tools.handler;

import java.util.Map;

/**
 * 策略模式：处理器上下文
 *
 * @Author: Mali
 * @Date: 2019/9/19 15:11
 * @Version 1.0
 */
public class HandlerContext {

    // 将注解中的类型值作为key，对应的类作为value，保存在Map中
    private Map<String, Class> handlerMap;

    // 通过构造方法将所有订单类型传入
    public HandlerContext(Map<String, Class> handlerMap) {
        this.handlerMap = handlerMap;
    }

    // 根据类型获取对应处理器
    @SuppressWarnings("unchecked")
    public AbstractHandler getInstance(String type) {
        Class clazz = handlerMap.get(type);
        if (null == clazz) {
            throw new IllegalArgumentException("not found handler type for: " + type);
        }
        return (AbstractHandler) BeanTool.getBean(clazz);
    }
}
