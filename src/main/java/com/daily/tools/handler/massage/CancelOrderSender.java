package com.daily.tools.handler.massage;

import com.daily.tools.enums.OrderQueueEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.MessagePostProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 取消订单消息的发送者（生产者）
 */
@Component
@Slf4j
public class CancelOrderSender {

    @Autowired
    private AmqpTemplate amqpTemplate;

    /**
     * 发送消息到消息队列
     *
     * @param code      订单编号
     * @param delayTime 延迟时间
     */
    public void sendMessage(String code, final long delayTime) {
        log.debug("向队列发送消息，订单编号：" + code);

        // 创建消息后处理器，设置订单过期时间
        MessagePostProcessor messagePostProcessor = message -> {
            message.getMessageProperties().setExpiration(String.valueOf(delayTime));
            return message;
        };
        // 将消息发送到“死信队列”中
        amqpTemplate.convertAndSend(OrderQueueEnum.QUEUE_TTL_ORDER_CANCEL.getName(), (Object) code, messagePostProcessor);
    }

}
