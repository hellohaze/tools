package com.daily.tools.handler.massage;

import com.daily.tools.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 取消订单消息的接收者（消费者）
 */
@Component
@Slf4j
public class CancelOrderReceive {

    @Autowired
    private OrderService orderService;

    // 监听“订单取消队列”，如果到期就会执行删除操作
    @RabbitListener(queues = "malls.order.cancel")
    public void handler(String msg) {
        log.debug("坚挺到要删除订单，id：" + msg);
        // 删除订单
        orderService.cancelOrder(msg);
    }

}
