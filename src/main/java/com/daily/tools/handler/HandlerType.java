package com.daily.tools.handler;

import java.lang.annotation.*;

/**
 * 策略模式：自定义注解
 *
 * @Author: Mali
 * @Date: 2019/9/19 14:58
 * @Version 1.0
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface HandlerType {

    String value();
}
