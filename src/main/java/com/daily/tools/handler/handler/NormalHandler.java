package com.daily.tools.handler.handler;

import com.daily.tools.bean.OrderDto;
import com.daily.tools.handler.AbstractHandler;
import com.daily.tools.handler.HandlerType;
import org.springframework.stereotype.Component;

/**
 * 策略模式：常规订单处理器
 * @Author: Mali
 * @Date: 2019/9/19 15:06
 * @Version 1.0
 */
@Component
@HandlerType("1")
public class NormalHandler extends AbstractHandler {
    @Override
    public String handle(OrderDto orderDto) {
        return "处理常规订单";
    }
}
