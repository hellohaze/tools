package com.daily.tools.enums;

import lombok.Getter;

/**
 * 消息队列枚举
 */
@Getter
public enum OrderQueueEnum {

    /**
     * 订单消息队列
     */
    QUEUE_ORDER_CANCEL("malls.order.direct", "malls.order.cancel", "malls.order.cancel"),

    /**
     * 订单到期未支付队列（死信队列）
     */
    QUEUE_TTL_ORDER_CANCEL("malls.order.direct.ttl", "malls.order.cancel.ttl", "malls.order.cancel.ttl");

    /**
     * 交换机
     */
    private String exchange;

    /**
     * 队列名称
     */
    private String name;

    /**
     * 路由键
     */
    private String routeKey;

    OrderQueueEnum(String exchange, String name, String routeKey) {
        this.exchange = exchange;
        this.name = name;
        this.routeKey = routeKey;
    }
}
