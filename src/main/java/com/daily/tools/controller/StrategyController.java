package com.daily.tools.controller;

import com.alibaba.fastjson.JSONObject;
import com.daily.tools.bean.OrderDto;
import com.daily.tools.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 策略模式：test controller
 * @Author: Mali
 * @Date: 2019/9/19 15:45
 * @Version 1.0
 */
@Controller
@RequestMapping("strategy")
public class StrategyController {

    @Autowired
    private OrderService orderService;

    @RequestMapping("handleOrder")
    @ResponseBody
    public Object handleOrder(String type) {
        OrderDto orderDto = new OrderDto();
        orderDto.setType(type);
        String result = orderService.handleOrder(orderDto);

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("result", result);
        return jsonObject;
    }
}
