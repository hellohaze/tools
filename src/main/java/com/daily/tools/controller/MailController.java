package com.daily.tools.controller;

import com.daily.tools.service.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Description:
 * Author: Mali
 * Date: 2018/11/22 14:51
 * Version:1.0
 **/
@Controller
@RequestMapping("mail")
public class MailController {

    @Autowired
    private MailService mailService;

    @PostMapping("sendSimpleMail")
    public void sendSimpleMail() {

    }
}
