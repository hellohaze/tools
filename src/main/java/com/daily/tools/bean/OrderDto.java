package com.daily.tools.bean;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 策略模式：test bean
 * @Author: Mali
 * @Date: 2019/9/19 15:04
 * @Version 1.0
 */
@Data
public class OrderDto {

    private String code;// 订单编号

    private BigDecimal price;// 金额

    private String type;// 订单类型：1.常规订单 2.团购订单 3.促销订单
}
