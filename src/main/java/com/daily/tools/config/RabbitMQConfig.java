package com.daily.tools.config;

import com.daily.tools.enums.OrderQueueEnum;
import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class RabbitMQConfig {

    /**
     * 订单消息实际消费队列所绑定的交换机
     *
     * @return
     */
    @Bean
    public DirectExchange directExchange() {
        return (DirectExchange) ExchangeBuilder
                // 指定交换机
                .directExchange(OrderQueueEnum.QUEUE_ORDER_CANCEL.getExchange())
                // 持久化
                .durable(true)
                .build();
    }

    /**
     * 订单消息实际消费队列
     *
     * @return
     */
    @Bean
    public Queue orderQueue() {
        return new Queue(OrderQueueEnum.QUEUE_ORDER_CANCEL.getName(), true, false, false);
    }

    /**
     * 死信队列
     *
     * @return
     */
    @Bean
    public Queue orderTTLQueue() {
        // 创建死信队列配置信息
        Map<String, Object> args = new HashMap<>();
        // 到期时间
        // args.put("x-message-ttl", 3600 * 1000);
        // 死信交换机
        args.put("x-dead-letter-exchange", OrderQueueEnum.QUEUE_ORDER_CANCEL.getExchange());
        // 死信路由键
        args.put("x-dead-letter-routing-key", OrderQueueEnum.QUEUE_ORDER_CANCEL.getRouteKey());

        // 填入死信队列配置信息并返回
        return new Queue(OrderQueueEnum.QUEUE_TTL_ORDER_CANCEL.getName(), true, false, false, args);
    }

    /**
     * 队列绑定交换机
     *
     * @return
     */
    @Bean
    public Binding orderBinding() {
        return BindingBuilder
                // 要绑定的队列
                .bind(orderQueue())
                // 绑定到的交换机
                .to(directExchange())
                // 指定路由键
                .with(OrderQueueEnum.QUEUE_ORDER_CANCEL.getRouteKey());
    }

}
